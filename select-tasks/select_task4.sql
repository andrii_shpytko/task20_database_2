show schemas;
use Labor_SQL;
show tables;

#1
select distinct maker
	from product 
		where type in ('pc');
#1a
select distinct maker, model, type 
	from product 
		where type in ('pc');
#1b
select maker, model, type 
	from product 
		where maker 
			not in (select maker from product where type = 'laptop') 
            and maker in (select maker from product where type = 'pc') 
            and type = 'pc';
#1c
select maker, model, type 
	from product 
		where maker 
			not in (select maker from product where type = 'pc') 
			and maker in (select maker from product where type = 'laptop') 
			and type = 'laptop';
#1d
select * 
	from product 
		where maker
            not in (select maker from product where type = 'pc')
			and maker in (select maker from product where type = 'printer')
			and type = 'printer';

#2
select maker, model, type 
	from product 
		where type = 'pc';
#2a
select maker, model, type 
	from product 
		where type = 'pc' ;
#2b
select maker, model, type 
	from product 
		where
			maker != all (select maker from product where type = 'laptop')
            and type = 'pc' 
            order by model desc;
#2c
select maker, model, type 
	from product 
		where 
			maker <> all (select maker from product where type = 'pc') 
            and type = 'laptop' 
		order by model desc;
#2d
select maker, model, type 
	from product 
		where 
			maker <> all (select maker from product where type = 'laptop')
            and maker <> all (select maker from product where type = 'pc')
            and type = 'printer' 
		order by model desc;
#2e
select maker, model, type 
	from product 
		where 
			maker <> all (select maker from product where type = 'printer')
            and maker != all (select maker from product where type  = 'pc') 
            and type = 'laptop'
		order by model desc;

#3
select maker, model, type 
	from product 
		where 
			maker = any (select maker from product where type = 'pc') 
            and maker <> all (select maker from product where type = 'laptop')
            and type = 'pc' 
		order by model desc;
#3a
select maker, model, type 
	from product 
		where 
			maker = any (select maker from product where type = 'laptop')
            and maker != all (select maker from product where type = 'pc')
            and type = 'laptop' 
		order by model desc;
#3b
select maker, model, type 
	from product 
		where 
			maker = any (select maker from product where type = 'printer') 
            and maker <> all (select maker from product where type = 'pc') 
			and type = 'printer' 
		order by model desc;
#3c
select maker, model, type 
	from product 
		where 
			maker  = any (select maker from product where type = 'printer') 
            and maker <> all (select maker from product where type = 'laptop') 
            and type = 'printer' 
		order by model asc;

#4
select maker, model, type 
	from product 
		where 
			maker in (select maker from product where type = 'laptop') 
            and maker in (select maker from product where type = 'pc') 
            and maker <> all (select maker from product where type = 'printer');
#4a
select maker, model, type 
	from product 
		where 
			maker in (select maker from product where type = 'laptop') 
            and maker in (select maker from product where type = 'printer') 
            and maker <> all (select maker from product where type = 'pc') 
		order by model desc;
#4b
select maker, model, type 
	from product 
		where 
			maker in (select maker from product where type = 'pc') 
            and maker in (select maker from product where type = 'laptop') 
            and maker <> all (select maker from product where type = 'printer')
		order by model desc;

#5
select maker, model, type 
	from product 
		where 
			maker <> all (select maker from product where type in ('printer')) 
		order by model desc;
#5a
select maker, model, type 
	from product 
		where 
			maker != all (select maker from product where type in ('pc')) 
		order by model desc;
#5b
select maker, model, type 
	from product 
		where 
			maker != all (select maker from product where type in ('laptop')) 
		order by model desc;

#6
select maker, model, type 
	from product 
		where 
			maker = any (select maker from product where type in ('pc', 'laptop')) 
            and maker not in (select maker from product where type = 'printer') 
		order by model desc;
#6a
select maker, model, type 
	from product 
		where 
			maker = any (select maker from product where type in ('pc')) 
            and maker  = any (select maker from product where type in ('laptop')) 
		order by model desc;
#6b
select maker, model, type 
	from product 
		where 
			maker = any (select maker from product where type in ('laptop', 'printer')) 
            and maker not in (select maker from product where type in ('pc')) 
		order by model desc;

#7
select maker, model, type 
	from product 
		where type = 'pc' 
        and model in (select model from pc);
#7a
select maker, model, type 
	from product 
		where type in ('pc', 'laptop') 
	order by maker asc, type desc;
#7b        
select maker, model, type 
	from product 
		where type in ('pc', 'printer') 
	order by maker asc, type desc;
#7c
select maker, model, type 
	from product 
		where type not in ('pc', 'printer') 
        and model in (select model from laptop) 
	order by maker asc, type desc;

#8
select country, class 
	from classes 
		where country = 'ukraine' or country != 'ukraine' and country != 'usa'
	order by country desc, class asc;
	
#10
select count(model) 
	from product 
		where maker = 'a' and type in ('pc');
select count(model) as count_model
	from product 
		where maker != 'a' and maker != 'b' and type in ('pc', 'laptop'); 

#11
select maker, model, type 
	from product 
		where type = 'pc' 
			and model != all (select model from pc);
select maker, model, type 
	from product 
		where type != 'pc' 
			and model != all (select model from pc) 
		order by maker asc, type desc;

#12
select model, price 
	from laptop
		where price > all (select price from pc);

select p.model, l.price 
	from laptop as l 
		join product as p on l.model = p.model 
		where price > all (select price from pc);

select p.model, l.price 
	from laptop as l 
		join product as p on l.model = p.model 
		where price > all (select price from pc) 
		and price > all(select price from printer);




