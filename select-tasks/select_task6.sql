show schemas;
use Labor_SQL;
show tables;

#1
select avg(price) as 'середня ціна' 
	from laptop;
select avg(price) as 'середня ціна' 
	from printer;
select avg(price) as 'середня ціна' 
	from pc;

#2
select concat('id: ', code) as 'code', 
	concat('model: ', model) as 'model', 
	concat('speed: ', speed) as 'speed', 
    concat('ram: ', ram) as 'ram', 
    concat('hd: ', hd) as 'hd', 
    concat('cd: ', cd) as 'cd', 
    concat('price: ', price, '$') as 'price'
from pc 
order by speed desc;

#3
select concat(year(date), '.', month(date), '.', day(date)) as 'date' 
from income;
#3a
select concat(year(date), '.', month(date), '.', day(date)) as 'date',
	concat('money: ', inc, '$') as 'inc'
from income 
	order by date desc, inc desc;

#5
select concat('ряд: ', id_psg, ' місце: ', place) as 'ряд'
	from pass_in_trip 
    order by id_psg desc, place asc;

#6
select concat('from ', town_from, ' to ', town_to) as 'trip'
from trip;
#6a
select concat('from ', town_from, ' to ', town_to) as 'trip', 
	concat('plane: ', plane) as 'plane'
	from trip 
		where plane in('boeing', 'il-86') and town_from = 'paris';

#4
select ship, result, case 
	when result = 'ok' then 'все добре' 
    when result = 'sunk' then 'потопленно' 
    when result = 'damaged' then 'пошкоджено' 
    else 'не визначено' 
	end as result_ua
from outcomes 
	order by result desc, ship asc;






