show schemas;
use Labor_SQL;
show tables;

#1
select model, price 
	from printer 
		where price = (select max(price) from printer);
#1a        
select model, price 
	from printer 
		where price = (select min(price) from printer); 
#1b
select model, price 
	from pc 
		where price = (select max(price) from pc);

#2
select 'laptop' as 'type', model, speed 
	from laptop 
		where speed < (select min(speed) from pc);
#2a
select 'pc' as 'type', model, speed, price 
	from pc where speed < (select max(speed) from pc) 
		order by price desc, model asc;
#2b
select 'printer' as 'type', model, price 
	from printer 
		where price < (select max(price) from pc) 
	order by price desc, model asc;

#3
select p.maker, pr.price, pr.type
	from product as p 
		join printer as pr on p.model = pr.model 
	where color = 'y'
		and price = (select min(price) from printer where color = 'y');
#3a
select p.maker, pr.price, pr.type
	from product as p 
		join printer as pr on p.model = pr.model 
			where color = 'n' 
				and price = (select max(price) from printer where color = 'n');

#4
select maker, type,  count(pc.model) 
	from product as p 
		join pc on p.model = pc.model 
	group by maker; 
#4a    
select maker, type, count(l.model) 
	from product as p 
		join laptop as l on p.model = l.model 
	group by maker; 

#5
select maker, avg(hd) 
	from product as p 
		join pc on p.model = pc.model 
			where maker = any ( select maker 
				from product 
					where maker in (select maker from product where type = 'pc') 
                    and maker in (select maker from product where type = 'printer')) 
		group by maker;
#5a
select maker, avg(hd) 
	from product as p 
		join laptop as l on p.model = l.model 
	where maker = any (select maker 
		from product 
			where maker in (select maker from product where type = 'laptop') 
            and maker in (select maker from product where type = 'printer')) 
		group by maker;

#6
select date, count(t.trip_no) 
	from pass_in_trip 
		join trip t on pass_in_trip.trip_no = t.trip_no
	where town_from = 'London'
	group by date 
    order by date desc;
#6a
select date, count(t.trip_no) 
	from pass_in_trip 
		join trip t on pass_in_trip.trip_no = t.trip_no
	where town_from = 'paris' 
	group by date 
    order by date desc;

#7
select date, count(t.trip_no) 
	from pass_in_trip 
		join trip t on pass_in_trip.trip_no = t.trip_no 
			where town_to = 'Moscow' 
    group by date;
#7a
select date, count(t.trip_no), town_to 
	from pass_in_trip 
		join trip t on pass_in_trip.trip_no = t.trip_no 
			where town_to in('Moscow', 'paris')
    group by date 
    order by date desc;

#8
select country, max(quantity_launched) as 'quantity_launched', launched 
	from (
		select country, launched, count(launched) as 'quantity_launched' 
			from ships as s
				join classes as c on s.class = c.class 
			group by country, launched
	) as ships_launched 
	group by country 
    order by country asc;

#9
select battle, count(ship)


select date, count(t.trip_no) 
	from pass_in_trip 
		join trip t on pass_in_trip.trip_no = t.trip_no
	where town_from = 'London'
	group by date 
    order by date desc;


#1c
select p.model, p.price, pr.type 
	from printer as p 
		join product as pr on p.model = pr.model 
			where price = (select max(price) from printer) 
union 
    select pc.model, pc.price, pr.type 
	from pc
		join product as pr on pc.model = pr.model 
			where price = (select min(price) from pc);