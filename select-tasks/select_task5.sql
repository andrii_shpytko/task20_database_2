show schemas;
use Labor_SQL;
show tables;

#1
select maker, model, type 
	from product as p 
		where exists ( 
			select * from pc 
				where p.model = pc.model 
        ) 
        order by model;
#1a
select maker, model, type 
	from product as p 
		where exists (
			select * from laptop as l 
				where p.model = l.model
        ) 
        order by model desc;
#1b
select maker, model, type 
	from product as p 
		where exists (
			select * from printer as pr 
				where p.model = pr.model
        ) 
        order by model desc;

#2
select maker, model, type
	from product as p 
		where exists (
			select * from pc where p.model = pc.model and pc.speed >= 750
		) 
        order by model desc;
#2a
select maker, model, type
	from product as p 
		where exists (
			select * from laptop as l where p.model = l.model and l.speed >= 750
        ) 
        order by model desc;

#4
select p.maker, p.model, p.type
	from product as p 
		join pc on pc.model = p.model 
			where pc.speed = ( 
				select max(speed) from pc 
            ) 
            and p.maker in (
				select maker from product where type = 'printer'
            );
#4a
select p.maker, p.model, p.type, l.price 
	from product as p 
		join laptop as l on p.model = l.model 
			where l.speed = ( 
				select max(speed) from laptop 
            ) 
            and p.maker in ( 
				select maker from product where type = 'pc' 
            ) 
		order by l.price desc; 
#4b
select p.maker, p.model, p.type, pc.price 
	from product as p 
		join pc on p.model = pc.model 
			where pc.speed = (
				select max(speed) from pc
            ) 
            and p.maker in (
				select maker from product where type = 'pc'
            ) ;


#7
select distinct maker, model, type 
	from product as p
		where type = 'laptop'
        and exists (select maker 
			from product where type = 'printer' 
			and maker = p.maker) 
		order by p.model desc;
#7a
select distinct maker, model, type 
	from product as p_laptop 
		where type = 'laptop' 
        and exists (select maker 
			from product where type = 'pc' 
            and p_laptop.maker = maker) 
		order by model desc;
#7b
select distinct maker, model, type 
	from product as pc_product 
		where type = 'pc' 
        and exists (select maker 
			from product where type = 'printer' 
            and maker = pc_product.maker) 
		order by model desc;

#8
select distinct maker, model, type 
	from product as laptop_product 
		where type = 'laptop' 
        and not exists (select maker 
			from product where type = 'printer' 
            and maker = laptop_product.maker) 
		order by model desc;
#8a
select distinct maker, model, type 
	from product as laptop_product 
		where type = 'laptop' 
        and not exists (select maker 
			from product where type = 'pc' 
            and laptop_product.maker = maker) 
		order by model desc;
#8b
select distinct maker, model, type 
	from product as printer_product 
		where type = 'printer' 
        and not exists (select maker 
			from product where type = 'pc' 
            and maker = printer_product.maker) 
		order by model desc;

#5
select s.name, s.launched, c.displacement, c.type 
	from ships as s 
		join Classes as c on s.class = c.class 
			where launched >= 1922 and displacement > 35000 
		order by s.launched desc;
#5a
select s.name, s.launched, c.displacement, c.type 
	from ships as s 
		join classes as c on s.class = c.class 
			where s.launched <= 1922 and c.displacement > 30000 and c.type = 'bc' 
		order by s.launched desc, s.name asc;
#5b
select s.name, s.launched, c.displacement, c.type 
	from ships as s 
		join classes as c on s.class = c.class 
			where c.numGuns > 7 and s.launched between 1900 and 1930 and c.type = 'bb' 
		order by s.launched desc, s.name asc;

#6
select c.class, s.name, s.launched  
	from outcomes as o 
		join ships as s on o.ship = s.name 
        join classes as c on s.class = c.class 
			where result = 'sunk';
#6a
select c.class, s.name, s.launched  
	from outcomes as o 
		join ships as s on o.ship = s.name 
        join classes as c on s.class = c.class 
			where result in ('ok', 'damaged');


